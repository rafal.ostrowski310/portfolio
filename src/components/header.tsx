/** @jsx jsx */

import { jsx, Link } from 'theme-ui';
import Headroom from 'react-headroom';

const Header = () => (
  <header>
    <Headroom>
      <div
        sx={{
          display: 'flex',
          flexDirection: 'row-reverse'
        }}
      >
        <Link href="#!" sx={{ variant: 'links.nav' }}>
          Hello
        </Link>
        <Link href="#!" sx={{ variant: 'links.nav' }}>
          World
        </Link>
      </div>
    </Headroom>
  </header>
);

export default Header;
