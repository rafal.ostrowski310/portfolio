import React from 'react';
import { graphql } from 'gatsby';
import { MDXRenderer } from 'gatsby-plugin-mdx';
import Img from 'gatsby-image';

interface Props {
  data: PropsData;
}

interface PropsData {
  mdx: ProjectMdx;
}

interface ProjectMdx {
  id: string;
  body: string;
  frontmatter: ProjectFrontmatter;
}

interface ProjectFrontmatter {
  title: string;
  featuredImage: any;
}

const ProjectPageTemplate = ({ data: { mdx } }: Props) => {
  const featuredImg = mdx.frontmatter.featuredImage?.childImageSharp?.fluid;

  return (
    <div>
      <h1>{mdx.frontmatter.title}</h1>
      <Img fluid={featuredImg} />
      <MDXRenderer>{mdx.body}</MDXRenderer>
    </div>
  );
};

export const pageQuery = graphql`
  query ProjectPageQuery($id: String) {
    mdx(id: { eq: $id }) {
      id
      body
      frontmatter {
        title
        featuredImage {
          childImageSharp {
            fluid(maxWidth: 800) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`;

export default ProjectPageTemplate;
