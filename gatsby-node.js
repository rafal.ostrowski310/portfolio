const path = require('path');

exports.createPages = async ({ graphql, actions, reporter }) => {
  // Destructure the createPage function from the actions object
  const { createPage } = actions;
  const result = await graphql(`
    query {
      allMdx {
        edges {
          node {
            id
            frontmatter {
              slug
            }
          }
        }
      }
    }
  `);
  if (result.errors) {
    reporter.panicOnBuild('🚨  ERROR: Loading "createPages" query');
  }
  // Create projects pages.
  const projects = result.data.allMdx.edges;
  // you'll call `createPage` for each result
  projects.forEach(({ node }, index) => {
    createPage({
      path: node.frontmatter.slug,
      // This component will wrap our MDX content
      component: path.resolve('./src/components/project-page-template.tsx'),
      // You can use the values in this context in
      // our page layout component
      context: { id: node.id }
    });
  });
};
